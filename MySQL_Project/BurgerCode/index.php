<!--
@Author: klein
@Date:   2018-06-19T15:05:52+02:00
@Email:  https://allan-fk.gitlab.io
@Filename: index.html
@Last modified by:   klein
@Last modified time: 2018-06-19T15:07:01+02:00
@License: GNU General Public License
-->



<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Burger Code</title>
    <!-- CSS -->
    <link rel="stylesheet" href="css/styles.css">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
    <!--Jquery -->
    <script src="https://code.jquery.com/jquery-1.11.3.js" integrity="sha256-IGWuzKD7mwVnNY01LtXxq3L84Tm/RJtNCYBfXZw3Je0=" crossorigin="anonymous"></script>
    <!-- bootstrap css -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.css">
    <!-- bootstrap js -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.js"></script>
  </head>
  <body>
    <div class="container site">
      <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span> Burger Code <span class="glyphicon glyphicon-cutlery"></span></h1>
      <nav>
        <ul class="nav nav-pills">
          <li role="presentation" class="active"><a href="#1" data-toggle="tab">Menus</a></li>
          <li role="presentation"><a href="#2" data-toggle="tab">Burgers</a></li>
          <li role="presentation"><a href="#3" data-toggle="tab">Snacks</a></li>
          <li role="presentation"><a href="#4" data-toggle="tab">Salades</a></li>
          <li role="presentation"><a href="#5" data-toggle="tab">Boissons</a></li>
          <li role="presentation"><a href="#6" data-toggle="tab">Desserts</a></li>
        </ul>
      </nav>
      <div class="tab-content">
        <div class="tab-pane active" id="1">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/m6.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Menu Classic 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="2">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 1</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/b6.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Burger 6</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="3">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/s1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Snacks 1</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/s2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Snacks 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/s3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Snacks 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/s4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Snacks 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/s5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Snacks 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="4">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/sa1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Salades 1</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/sa2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Salade 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/sa3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Salade 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/sa4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Salade 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/sa5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Salade 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="5">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 1</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/bo6.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Boisson 6</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane" id="6">
          <div class="row">
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/d1.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Dessert 1</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/d2.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Dessert 2</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/d3.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Dessert 3</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/d4.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Dessert 4</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
            <div class="col-sm-6 col-md-4">
              <div class="thumbnail">
                <img src="images/d5.png" alt="...">
                <div class="price">8.90€</div>
                <div class="caption">
                  <h4>Dessert 5</h4>
                  <p>Sandwich: Burger, Salade, Tomate, Cornichon + Frites + Boisson</p>
                  <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </body>
</html>
