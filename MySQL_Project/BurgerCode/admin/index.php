<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Burger Code</title>
    <!-- CSS -->
    <link rel="stylesheet" href="../css/styles.css">
    <!-- FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
    <!--Jquery -->
    <script src="https://code.jquery.com/jquery-1.11.3.js" integrity="sha256-IGWuzKD7mwVnNY01LtXxq3L84Tm/RJtNCYBfXZw3Je0=" crossorigin="anonymous"></script>
    <!-- bootstrap css -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- bootstrap js -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </head>
  <body>
    <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span> Burger Code <span class="glyphicon glyphicon-cutlery"></span></h1>
    <div class="container admin">
      <div class="row">
        <h1><strong>Liste des items</strong><a href="insert.php" class="btn btn-success btn-lg"><span class="glyphicon glyphicon-plus"></span>Ajouter</a></h1>
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>Nom</th>
              <th>Description</th>
              <th>Prix</th>
              <th>Catégorie</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            <?php
              require 'database.php';
              $db=Database::connect();
              $statement = $db->query('SELECT items.id, items.name, items.description, items.price, categories.name AS category
              FROM items LEFT JOIN categories ON items.category = categories.id
              ORDER BY items.id DESC');
              while($item = $statement->fetch())
              {
                echo '<tr>';
                echo '<td>' . $item['name'] . '</td>';
                echo '<td>' . $item['description'] . '</td>';
                echo '<td>' . number_format((float)$item['price'],2,'.','') . '€' . '</td>';
                echo '<td>' . $item['category'] . '</td>';
                echo '<td width=300>';
                echo  '<a href="view.php?id=' . $item['id'] . '"class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span>Voir</a>';
                echo  '<a href="update.php?id=' . $item['id'] . '"class="btn btn-primary"><span class="glyphicon glyphicon-eye-pencil"></span>Modifier</a>';
                echo  '<a href="delete.php?id=' . $item['id'] . '"class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Supprimer</a>';
                echo '</td>';
                echo '</tr>';
              }
              Database::disconnect();
            ?>
            <tr>
              <td>Item 2</td>
              <td>Description 2</td>
              <td>Prix 2</td>
              <td>Catégorie 2</td>
              <td width=300>
                <a href="view.php?id=2" class="btn btn-default"><span class="glyphicon glyphicon-eye-open"></span>Voir</a>
                <a href="update.php?id=2" class="btn btn-primary"><span class="glyphicon glyphicon-eye-pencil"></span>Modifier</a>
                <a href="delete.php?id=2" class="btn btn-danger"><span class="glyphicon glyphicon-remove"></span>Supprimer</a>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </body>
</html>
