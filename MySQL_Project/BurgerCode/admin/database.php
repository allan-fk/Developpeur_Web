<?php
# @Author: klein
# @Date:   2018-06-20T13:08:25+02:00
# @Email:  https://allan-fk.gitlab.io
# @Filename: database.php
# @Last modified by:   klein
# @Last modified time: 2018-06-20T13:33:49+02:00
# @License: GNU General Public License

class Database
{
  private static $dbHost = "localhost";
  private static $dbName = "burger_code";
  private static  $dbUser = "root";
  private static  $dbUserPassword = "";
  private static $connection = null;

  public static function connect() {
    try {
      self::$connection = new PDO("mysql:host=" . self::$dbHost . ";dbname=" . self::$dbName,self::$dbUser,self::$dbUserPassword);
    }
    catch (PDOException $e) {
      die($e->getMessage);
    }
    return self::$connection;
  }

  public static function disconnect() {
    self::$connection = null;
  }
}

// Database::connect();




?>
