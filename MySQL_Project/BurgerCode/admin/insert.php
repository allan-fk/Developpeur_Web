<?php
# @Author: klein
# @Date:   2018-06-20T13:36:30+02:00
# @Email:  https://allan-fk.gitlab.io
# @Filename: insert.php
# @Last modified by:   klein
# @Last modified time: 2018-06-20T13:36:30+02:00
# @License: GNU General Public License
  require 'database.php';
  $nameError = $descriptionError = $priceError = $categoryError = $imageError = $name = $description = $price = $image = "";
  if(!empty($_POST)) {
    $name = checkInput($_POST['name']);
    $description = checkInput($_POST['description']);
    $price = checkInput($_POST['price']);
    $category = checkInput($_POST['category']);
    $image = checkInput($_FILES['image']['name']);
    $imagePath ='../images' .basename ($image);
    $imageExtension =pathinfo($infoPath, PATHINFO_EXTENSION);
    $isSuccess = true;
    $isUploadSucces = false;

    if (empty($name)) {
      $nameError = 'Ce champ ne peut pas être vide';
      $isSuccess = false;
    }
    if (empty($description)) {
      $descriptionError = 'Ce champ ne peut pas être vide';
      $isSuccess = false;
    }
    if (empty($price)) {
      $priceError = 'Ce champ ne peut pas être vide';
      $isSuccess = false;
    }
    if (empty($category)) {
      $categoryError = 'Ce champ ne peut pas être vide';
      $isSuccess = false;
    }
    if (empty($image)) {
      $imageError = 'Ce champ ne peut pas être vide';
      $isSuccess = false;
    }
    else {
      $isUploadSuccess = true;
      if($imageExtension != "jpg" && $imageExtension != "png" && $imageExtension != "jpeg" && $imageExtension != "gif" ) {
          $imageError = "Les fichiers autorises sont: .jpg, .jpeg, .png, .gif";
          $isUploadSuccess = false;
      }
      if(file_exists($imagePath)) {
          $imageError = "Le fichier existe deja";
          $isUploadSuccess = false;
      }
      if($_FILES["image"]["size"] > 500000) {
          $imageError = "Le fichier ne doit pas depasser les 500KB";
          $isUploadSuccess = false;
      }
      if($isUploadSuccess) {
          if(!move_uploaded_file($_FILES["image"]["tmp_name"], $imagePath)) {
              $imageError = "Il y a eu une erreur lors de l'upload";
              $isUploadSuccess = false;
          }
      }
    }
    if($isSuccess && $isUploadSuccess) {
      $db = Database::connect();
      $statement = $db->prepare("INSERT INTO items (name,description,price,category,image) values(?, ?, ?, ?, ?)");
      $statement->execute(array($name,$description,$price,$category,$image));
      Database::disconnect();
      header("Location: index.php");
    }
  }

  function checkInput($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);
    return $data;
}
?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Burger Code</title>
    <link rel="stylesheet" href="../css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.3.js" integrity="sha256-IGWuzKD7mwVnNY01LtXxq3L84Tm/RJtNCYBfXZw3Je0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </head>
  <body>
    <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span> Burger Code <span class="glyphicon glyphicon-cutlery"></span></h1>
    <div class="container admin">
      <div class="row">
        <h1><strong>Ajouter un item</strong></h1>
        <br>
          <form class="form" role="form" action="insert.php" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label for="name">Nom: </label>
              <input type="text" class="form-control" id="name" name="name" placeholder="Nom" value="<?php echo $name; ?>">
              <span class="help-inline"><?php echo $nameError; ?></span>
            </div>
            <div class="form-group">
              <label for="description">Description: </label>
              <input type="text" class="form-control" id="description" name="description" placeholder="Description" value="<?php echo $description; ?>">
              <span class="help-inline"><?php echo $descriptionError; ?></span>
            </div>
            <div class="form-group">
              <label for="price">Prix: (en €) </label>
              <input type="number" step="0.01" class="form-control" id="price" name="price" placeholder="Prix" value="<?php echo $price; ?>">
              <span class="help-inline"><?php echo $nameError; ?></span>
            </div>
            <div class="form-group">
              <label for="catégorie">Catégorie: </label>
              <select class="form-control" id="category" name="category" placeholder="Description" value="<?php
                $db=Database::connect();
                foreach($db->query('SELECT * FROM categories') as $row) {
                  echo '<option value="' . $row['id'] . '">' . $row['name'] . '</option>';
                }
                Database::disconnect();
              ?>">
              <span class="help-inline"><?php echo $descriptionError; ?></span>
            </div>
            <div class="form-group">
              <label for="image">Selectionner une image: </label>
              <input type="file" id="image" name="image">
              <span class="help-inline"><?php echo $imageError;?></span>
            </div>
            <br>
            <div class="form-actions">
              <button type="submit" class="btn btn-succes" name="button"><span class="glyphicon glyphicon-pencil"></span>Ajouter</button>
              <a href="index.php" class="btn btn-primary"><span class="glyphicon glyphicon-arrco-left"></span> Retour</a>
            </div>
        </form>
      </div>
    </div>
  </body>
</html>
