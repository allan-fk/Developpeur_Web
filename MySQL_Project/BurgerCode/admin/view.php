<?php
# @Author: klein
# @Date:   2018-06-20T13:37:27+02:00
# @Email:  https://allan-fk.gitlab.io
# @Filename: view.php
# @Last modified by:   klein
# @Last modified time: 2018-06-20T13:37:28+02:00
# @License: GNU General Public License
require 'database.php';

if(!empty($_GET['id'])) {
  $id = checkInput($_GET['id']);
}

$db = Database::connect();
$statement = $db->prepare("SELECT items.id, items.name, items.description, items.price, items.image, categories.name AS category
FROM items LEFT JOIN categories ON items.category = categories.id
WHERE items.id = ?");

$statement->execute(array($id));
$item = $statement->fetch();
Database::disconnect();

function checkInput($data) {
  $data = trim($data);
  $data = stripslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Burger Code</title>
    <link rel="stylesheet" href="../css/styles.css">
    <link href="https://fonts.googleapis.com/css?family=Holtwood+One+SC" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.11.3.js" integrity="sha256-IGWuzKD7mwVnNY01LtXxq3L84Tm/RJtNCYBfXZw3Je0=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
  </head>
  <body>
    <h1 class="text-logo"><span class="glyphicon glyphicon-cutlery"></span> Burger Code <span class="glyphicon glyphicon-cutlery"></span></h1>
    <div class="container admin">
      <div class="row">
        <div class="col-sm-6">
          <h1><strong>Voir un item</strong></h1>
          <br>
            <form action="">
              <div class="form-group">
                <label for="">Nom: </label><?php echo ' ' . $item['name'];?>
              </div>
              <div class="form-group">
                <label for="">Description: </label><?php echo ' ' . $item['description'];?>
              </div>
              <div class="form-group">
                <label for="">Prix: </label><?php echo ' ' . number_format((float)$item['price'],2,'.','') . ' €' . '</td>';?>
              </div>
              <div class="form-group">
                <label for="">Catégories: </label><?php echo ' ' . $item['category'];?>
              </div>
              <div class="form-group">
                <label for="">Image: </label><?php echo ' ' . $item['image'];?>
              </div>
            </form>
            <div class="form-actions">
              <a href="index.php" class="btn btn-primary"><span class="glyphicon glyphicon-arrco-left"></span> Retour</a>
            </div>
          </br>
        </div>
        <div class="col-sm-6 site">
          <div class="thumbnail">
            <img src="<?php echo '../images/' . $item['image'];?>" alt="...">
            <div class="price"><?php echo number_format((float)$item['price'],2,'.','') .  ' €' . '</td>';?></div>
            <div class="caption">
              <h4><?php echo $item['name'];?></h4>
              <p><?php echo $item['description'];?></p>
              <a href="#" class="btn btn-order" role="button"><span class="glyphicon glyphicon-shopping-cart"></span> Commander</a>
            </div>
          </div>
        </div>


      </div>
    </div>
  </body>
</html>
