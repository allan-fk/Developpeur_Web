window.onload = function() {
  // Var qui seront accesible dans tt les fonctions
  var canvas;
  var ctx;
  var delay = 200;
  var xCoord = 0;
  var yCoord = 0;

  init();

  // Initialisation
  function init() {
  canvas = document.createElement('canvas');
  // Style de notre canvas
  canvas.width = 900;
  canvas.height = 600;
  canvas.style.border = "2px solid black";
  // Placmenent du canvas dans la balise body
  document.body.appendChild(canvas);
  // Context permettent de dessiner sur le canvas
  ctx = canvas.getContext('2d');
  refreshCanvas();
  }

  function refreshCanvas() {
    // Style du canvas
    xCoord += 5;
    yCoord += 5;
    ctx.clearRect(0, 0, canvas.whidth, canvas.height);
    ctx.fillStyle = "#ff0000";
    ctx.fillRect(xCoord, yCoord, 100, 50);
    setTimeout(refreshCnnvas, delay);
  }
}
