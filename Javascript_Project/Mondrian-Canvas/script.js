window.onload = function() {
  // Var qui seront accesible dans tt les fonctions
  //var canvas;
  //var ctx;
  canvas = document.createElement('canvas');
  // Style de notre canvas
  canvas.width = 900;
  canvas.height = 900;
  canvas.style.border = "2px solid black";
  // Placmenent du canvas dans la balise body
  document.body.appendChild(canvas);
  // Context permettent de dessiner sur le canvas
  // Canvas 1
  ctx1 = canvas.getContext('2d');
  ctx1.fillStyle = "#ff0000";
  ctx1.lineWidth = 10;
  ctx1.strokeRect(-20, 515, 420, 520);
  ctx1.fillRect(0, 0, 400, 500);
  // Canvas 2
  ctx2 = canvas.getContext('2d');
  ctx2.fillStyle = "white";
  ctx2.lineWidth = 10;
  ctx2.strokeRect(-20, -15, 420, 520);
  ctx2.fillRect(0, 520, 400, 500);
  // Canvas 3
  ctx3 = canvas.getContext('2d');
  ctx3.fillStyle = "white";
  ctx3.lineWidth = 10;
  ctx3.strokeRect(405, -20, 600, 525);
  ctx3.fillRect(405, 0, 600, 500);
  // Canvas 4
  ctx4 = canvas.getContext('2d');
  ctx4.fillStyle = "white";
  ctx4.lineWidth = 10;
  ctx4.strokeRect(405, 510, 440, 360);
  ctx4.fillRect(410, 515, 430, 350);
  // Canvas 5
  ctx5 = canvas.getContext('2d');
  ctx5.fillStyle = "yellow";
  ctx5.lineWidth = 10;
  ctx5.strokeRect(405, 875, 310, 360);
  ctx5.fillRect(410, 875, 300, 280);
  // Canvas 6
  ctx5 = canvas.getContext('2d');
  ctx5.fillStyle = "blue";
  ctx5.lineWidth = 10;
  ctx5.strokeRect(850, 695, 400, 470);
  ctx5.fillRect(850, 700, 300, 280);
  // Canvas 6
  ctx5 = canvas.getContext('2d');
  ctx5.fillStyle = "black";
  ctx5.lineWidth = 10;
  ctx5.strokeRect(850, 695, 400, 470);
  ctx5.fillRect(850, 510, 250, 180);

}
